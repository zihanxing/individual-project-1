+++
title = "Home"
description = "Home site description"
sort_by = "date"
template = "index.html"
page_template = "page.html"
+++


{{ tab_nav_box(
        id = "home"
        class = "mb-5"
        tab_titles = [
            "👋 Hello",
            "✉ Contact"
        ]
        tab_contents = [
            "Hi, I am Zach Xing. A current student in the MEng AI program at Duke University",
            "Email: <a href='mailto:zx123@duke.edu'> zx123@duke.edu"
        ]
    )
}}

<!-- ## Patrocinio

[![Liberapay](https://img.shields.io/badge/Financia%20mi%20trabajo-F6C915?style=flat&logo=liberapay&logoColor=ffffff "Finance my work")](https://liberapay.com/gersonbenavides/donate)  [![PayPal](https://img.shields.io/badge/Realiza%20una%20donación-00457C?style=flat&logo=paypal "Make a donation")](https://paypal.me/gersonbdev?country.x=CO&locale.x=es_XC) -->


