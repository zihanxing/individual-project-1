# Individual Project 1

## Demo Link
[Individual Project 1](https://www.loom.com/share/58daab356a7c48aabafdde069b725e78?sid=0d5a8836-9912-4236-9d2c-a469384097b9)

## Project Objective
1. Website built with Zola Links to an external site., Hugo, Gatsby, Next.js or equivalent
2. GitLab workflow to build and deploy site on push
3. Hosted on Vercel, Netlify, AWS Amplify, AWS S3, or others.

## Implementation Phases
### Setting Up the Environment
1. For Ubuntu users, to install `Zola`:
    ```
    sudo snap install --edge zola
    ```
    If `snap` is not already installed, add it using:
    ```
    sudo apt install snapd
    ```
2. Confirm the successful installation of `Zola`:
    ```
    zola --version
    ```

### Website Creation and Layout with Zola
1. Initiate your Zola project with `zola init <YOUR_SITE>`
2. Incorporate a theme by cloning it into the theme directory:
    ```
    cd <YOUR_SITE>/themes
    git clone <GIT_THEME_URL>
    ```
3. Activate the theme in the config.toml file.
4. Craft your website's content.
5. Optionally, you can build your website with `zola build`.
6. Preview your website locally at http://127.0.0.1:1111 using `zola serve`.

### Automating Build and Deployment with GitLab
1. Make sure the theme is accessible by the runner:
    ```
    git submodule add <HTTP_THEME_URL> themes/<THEME_NAME>
    ```
2. Establish the **GitLab CI/CD pipeline** by creating a `.gitlab-ci.yml` file at the root of your project, including deployment instructions. The pipeline setup is as follows in `.gitlab-ci.yml`:
    ```yaml
    stages:
        - deploy

    default:
        image: debian:stable-slim
        tags:
            - docker

    variables:
        # Ensures theme accessibility by setting the submodule strategy to "recursive".
        GIT_SUBMODULE_STRATEGY: "recursive"

        # Specify the Zola version to use for building your site, or leave blank to use the latest version from GitHub releases.
        ZOLA_VERSION:
            description: "The Zola version for site building."
            value: ""

    pages:
        stage: deploy
        script:
            - |
            apt-get update --assume-yes && apt-get install --assume-yes --no-install-recommends wget ca-certificates
            if [ $ZOLA_VERSION ]; then
                zola_url="https://github.com/getzola/zola/releases/download/v$ZOLA_VERSION/zola-v$ZOLA_VERSION-x86_64-unknown-linux-gnu.tar.gz"
                if ! wget --quiet --spider $zola_url; then
                echo "Specified Zola version not found.";
                exit 1;
                fi
            else
                github_api_url="https://api.github.com/repos/getzola/zola/releases/latest"
                zola_url=$(
                wget --output-document - $github_api_url |
                grep "browser_download_url.*linux-gnu.tar.gz" |
                cut --delimiter : --fields 2,3 |
                tr --delete "\" "
                )
            fi
            wget $zola_url
            tar -xzf *.tar.gz
            ./zola build
    ```

### Deployment via Netlify
1. In your project's root, create `netlify.toml`.
2. Define deployment parameters in `netlify.toml` and commit your project to the GitLab repository. The deployment file content is as follows:
    ```toml
    [build]
    publish = "/public"
    command = "zola build"

    [build.environment]
    ZOLA_VERSION = "0.18.0"

    [context.deploy-preview]
    command = "zola build --base-url $DEPLOY_PRIME_URL"
    ```
3. Log into [Netlify](https://app.netlify.com/) and navigate to **Sites** to add a new site. Import your project from GitLab, where the `netlify.toml` configuration will automatically be applied.

## Screenshots
### Zola Development Environment
![Zola Development Environment](./static/screenshots/zola.png)
### Gitlab CI/CD Pipeline
![Gitlab CI/CD Pipeline](./static/screenshots/gitlab.png)
### Netlify Deployment
![Netlify Deployment](./static/screenshots/netlify.png)